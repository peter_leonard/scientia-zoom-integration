﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ScientiaZoomKit;
using SplusServer;

namespace ScientiaZoomKitUnitTest
{
    [TestClass]
    public class TestImageKit
    {
        private const string testProgId = "TESTZOOM.Application";
        private const string testHostKey = "zoom-integration";

        [TestMethod]
        public void Test_GetApplication()
        {
            var splus = ImageKit.GetApplication(testProgId);
            Assert.IsNotNull(splus);
        }

        [TestMethod]
        public void Test_GetTagIfItExists()
        {
            var splus = ImageKit.GetApplication(testProgId);
            var college = splus.ActiveCollege;

            List<string> validation = new List<string>();
            Tag tag = null;

            ImageKit.GetTagIfItExists(testHostKey, college, ref tag, validation);

            Assert.IsNotNull(tag);
            Assert.AreEqual(0, validation.Count);
        }

        [TestMethod]
        public void Test_GetModuleGroupIfExists()
        {
            var splus = ImageKit.GetApplication(testProgId);
            var college = splus.ActiveCollege;

            List<string> validation = new List<string>();
            ModuleGroup moduleGroup = null;

            ImageKit.GetModuleGroupIfExists(testHostKey, college, ref moduleGroup, validation);

            Assert.IsNotNull(moduleGroup);
            Assert.AreEqual(0, validation.Count);
        }

        [TestMethod]
        public void Test_GetActivityGroupIfExists()
        {
            var splus = ImageKit.GetApplication(testProgId);
            var college = splus.ActiveCollege;

            List<string> validation = new List<string>();
            ActivityGroup actGroup = null;

            ImageKit.GetActivityGroupIfExists(testHostKey, college, ref actGroup, validation);

            Assert.IsNotNull(actGroup);
            Assert.AreEqual(0, validation.Count);
        }

    }
}
