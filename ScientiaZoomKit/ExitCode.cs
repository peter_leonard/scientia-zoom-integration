﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ScientiaZoomKit
{
    public enum ExitCode : int
    {
        Success = 0,
        InvalidArgs = 1,
        ImageConnectionFailed = 2,
        SplusObjectNotFoundOrDuplicate = 3,
        UnknownError = 10
    }
}
