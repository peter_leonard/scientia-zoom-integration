﻿using Microsoft.IdentityModel.Tokens;
using Newtonsoft.Json;
using RestSharp;
using System;
using System.Text;
using SplusServer;

namespace ScientiaZoomKit
{
    public class ZoomKit
    {
        public static string getJwtToken(string apiSecret, string apiKey)
        {
            var tokenHandler = new System.IdentityModel.Tokens.Jwt.JwtSecurityTokenHandler();
            var now = DateTime.UtcNow;
            byte[] symmetricKey = Encoding.ASCII.GetBytes(apiSecret);

            var tokenDescriptor = new SecurityTokenDescriptor
            {
                Issuer = apiKey,
                Expires = now.AddSeconds(30),
                SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(symmetricKey), SecurityAlgorithms.HmacSha256),
            };
            var token = tokenHandler.CreateToken(tokenDescriptor);

            return tokenHandler.WriteToken(token);
        }

        public static dynamic getAccountProfile(string tokenString)
        {
            var client = new RestClient("https://api.zoom.us/v2/users?status=active&page_size=30&page_number=1");
            var request = new RestRequest(Method.GET);
            request.AddHeader("content-type", "application/json");
            request.AddHeader("authorization", String.Format("Bearer {0}", tokenString));

            IRestResponse response = client.Execute(request);
            dynamic json = JsonConvert.DeserializeObject(response.Content);

            return json;
        }

        private static string _requestBodyFormat(string title, string password, string description)
        {
            return "{\"topic\":\"" + title + "\",\"type\":\"3\"," +
                "\"password\":\"" + password + "\",\"agenda\":\"" + description + "\"," +
                "\"settings\":{\"host_video\":true,\"waiting_room\":true}}";
        }

        private static RestRequest _createHttpRequest(Activity act, StaffMember staff, string userId, string tokenString)
        {
            var request = new RestRequest(Method.POST);
            request.AddHeader("content-type", "application/json");
            request.AddHeader("authorization", String.Format("Bearer {0}", tokenString));

            var body = _requestBodyFormat(act.Name, "password", act.Description);
            request.AddParameter("application/json", body, ParameterType.RequestBody);

            return request;
        }

        public static void createMeeting(Activity act, StaffMember staff, string userId, string tokenString)
        {
            var url = string.Format("https://api.zoom.us/v2/users/{0}/meetings", userId);

            var client = new RestClient(url);
            var request = _createHttpRequest(act, staff, userId, tokenString);

            IRestResponse response = client.Execute(request);
            dynamic json = JsonConvert.DeserializeObject(response.Content);

            // TODO: Discuss where and how to update the start_url and join_url
            string start_url = json.start_url;
            string join_url = json.join_url;

            act.UserText1 = start_url;
            act.UserText2 = join_url;
        }
    }
}
