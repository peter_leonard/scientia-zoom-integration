﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using SplusServer;

namespace ScientiaZoomKit
{
    public class ImageKit
    {
        public static Application GetApplication(string imageName)
        {
            try
            {

                dynamic splus = Activator.CreateInstance(Type.GetTypeFromProgID(imageName, true));
                return (SplusServer.Application)splus;
            }
            catch (Exception e)
            {
                string message = String.Format("ERROR: Cannot instantiate SPlusServer() pointing to '{0}'. ", imageName);
                message += String.Format("Ensure that the SPlus image file for {0} exists ", imageName);
                message += "and is registered for COM on this machine";
                throw new Exception(String.Format(message), e);
            }
        }

        public static void GetTagIfItExists(string hostKey, College college, ref Tag tag, List<string> validationErrors)
        {
            if (hostKey != null && hostKey != "*")
            {
                var activityTags = college.Tags.Find(hostKey);
                if (activityTags.Count > 1)
                    validationErrors.Add($"  * Duplicate Tag with HostKey '{hostKey}'");
                if (activityTags.Count == 0)
                    validationErrors.Add($"  * Can't find Tag with HostKey '{hostKey}'");
                if (activityTags.Count == 1)
                    tag = activityTags[1];
            }
        }

        public static void GetModuleGroupIfExists(string hostKey, College college, ref ModuleGroup moduleGroup, List<string> validationErrors)
        {
            if (hostKey != null && hostKey != "*")
            {
                var moduleGroups = college.ModuleGroups.Find(hostKey);
                if (moduleGroups.Count > 1)
                    validationErrors.Add($"  * Duplicate Module Groups with HostKey '{hostKey}'");
                if (moduleGroups.Count == 0)
                    validationErrors.Add($"  * Can't find Module Group with HostKey '{hostKey}'");
                if (moduleGroups.Count == 1)
                    moduleGroup = moduleGroups[1];
            }
        }

        public static void GetActivityGroupIfExists(string hostKey, College college, ref ActivityGroup activityGroup, List<string> validationErrors)
        {
            if (hostKey != null && hostKey != "*")
            {
                var activityGroups = college.ActivityGroups.Find(hostKey);
                if (activityGroups.Count > 1)
                    validationErrors.Add($"  * Duplicate Activity Groups with HostKey '{hostKey}'");
                if (activityGroups.Count == 0)
                    validationErrors.Add($"  * Can't find Activity Group with HostKey '{hostKey}'");
                if (activityGroups.Count == 1)
                    activityGroup = activityGroups[1];
            }
        }


        public static void GetActivityTemplateGroupIfExists(string hostKey, College college, ref ActivityTemplateGroup activityTemplateGroup, List<string> validationErrors)
        {
            if (hostKey != null && hostKey != "*")
            {
                var activityTemplateGroups = college.ActivityTemplateGroups.Find(hostKey);
                if (activityTemplateGroups.Count > 1)
                    validationErrors.Add($"  * Duplicate Activity Template Groups with HostKey '{hostKey}'");
                if (activityTemplateGroups.Count == 0)
                    validationErrors.Add($"  * Can't find Activity Template Group with HostKey '{hostKey}'");
                if (activityTemplateGroups.Count == 1)
                    activityTemplateGroup = activityTemplateGroups[1];
            }
        }

        public static bool checkPatternExistsFromUserText2(Activity act, Regex pattern)
        {
            if (pattern.IsMatch(act.UserText2))
            {
                return true;
            }
            return false;
        }
    }
}
