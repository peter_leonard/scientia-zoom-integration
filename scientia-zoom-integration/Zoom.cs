﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SplusServer;
using ScientiaZoomKit;
using static ZoomIntegration.Program;
using System.Text.RegularExpressions;

namespace ZoomIntegration
{
    class Zoom
    {
        private readonly Options _options;
        private readonly String apiKey = "PtPrhnELR8yEgDjtBva9GA";
        private readonly String secretKey = "yLsG3IWfqVytzWoM7fppti9FyJEcNoa9pU9O";
        private Dictionary<String, String> userDict = new Dictionary<String, String>();
        private List<Activity> _activityList = new List<Activity>();

        public Zoom(Options options)
        {
            _options = options;

        }

        public int Execute()
        {
            var preConnectionValidationErrors = PreConnectionValidation(_options);
            // First stage of validation (before connecting to the image)
            if (preConnectionValidationErrors.Any())
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.Error.WriteLine("Initial command validation succeeded, however further checks show error(s):");
                preConnectionValidationErrors.ForEach(e => Console.WriteLine(e));
                Console.Error.WriteLine();
                Console.Error.WriteLine("Additional help is available via the --help option.");
                Console.ResetColor();
                return (int)ExitCode.InvalidArgs;
            }

            // Connect to the image
            Console.WriteLine($"Connecting to Syllabus Plus Image {_options.ProgId}.");

            Application splus;
            College college;

            try
            {
                splus = ImageKit.GetApplication($"{_options.ProgId}.Application");
                college = splus.ActiveCollege;
            }
            catch (Exception)
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.Error.WriteLine($"  * Failed to connect to Syllabus Plus Image with ProgId = {_options.ProgId}");
                Console.Error.WriteLine($"  * Stopping now, can't continue. ");
                Console.ResetColor();
                return (int)ExitCode.ImageConnectionFailed;
            }

            Console.WriteLine($"Connected Successfully.");

            Console.WriteLine($"Refreshing Splus Image.");

            try
            {
                splus.SDB.Refresh();
            }
            catch (Exception e)
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.Error.WriteLine($"  * Failed to refresh Syllabus Plus Image [ProgId = {_options.ProgId}] - {e.Message}");
                Console.Error.WriteLine($"  * Stopping now, can't continue. ");
                Console.ResetColor();
                return (int)ExitCode.ImageConnectionFailed;
            }

            Console.WriteLine($"Splus Image refreshed.");

            var postConnectionValidationErrors = PostConnectionValidation(college);

            if (postConnectionValidationErrors.Any())
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.Error.WriteLine("Initial command validation succeeded, however further checks show error(s):");
                postConnectionValidationErrors.ForEach(e => Console.WriteLine(e));
                Console.Error.WriteLine();
                Console.Error.WriteLine("Additional help is available via the --help option.");
                Console.ResetColor();
                return (int)ExitCode.SplusObjectNotFoundOrDuplicate;
            }

            if (splus.MainWindow.State != cpWindowStateType.cpWindowStateTypeNormal ||
                splus.MainWindow.State != cpWindowStateType.cpWindowStateTypeMaximized)
            {
                Console.WriteLine("Minimising Syllabus Plus GUI window for speed.");
                splus.MainWindow.State = cpWindowStateType.cpWindowStateTypeIcon;
            }

            Console.WriteLine($"Disabling the auto problem checking feature.");
            // Disable problem checking (we don't want concurrency issues)
            college.AutoCheckForProblems = false;

            Console.WriteLine($"Manually checking for problems.");
            college.CheckForProblems();

            // TODO: How to configure the TAG KEY whether its user input? or Hard coded to the program?
            string tagKey = "Zoom-integration";

            // Get Activity by ActivityGroups that is tagged by tagKey.
            ActivityGroups actGroups = college.ActivityGroups;
            foreach (ActivityGroup actGroup in actGroups)
            {
                if (actGroup.AssociatedTags.Find(tagKey).Count > 0)
                {
                    Activities acts = actGroup.Members;
                    foreach (Activity act in acts)
                    {
                        if (!_activityList.Contains(act))
                        {
                            _activityList.Add(act);
                        }
                    }
                }
            }

            // TBD: Zoom Token created initially, or created everytime API get called.
            string tokenString = ZoomKit.getJwtToken(secretKey, apiKey);
            Console.WriteLine($"Temporary Token key created - {tokenString}");

            dynamic masterAccountProfile = ZoomKit.getAccountProfile(tokenString);

            foreach (dynamic user in masterAccountProfile.users)
            {
                userDict.Add((string)user.email, (string)user.id);
            }

            foreach (Activity act in _activityList)
            {
                if (checkZoomMeetingExistsFromUserText2(act))
                {
                    Console.WriteLine($"Zoom meeting for {act.Name} already exists.");
                }
                else
                {
                    // TODO: How to handle multiple staffs?
                    StaffMembers staffs = act.GetStaffMemberRequirementPresets();
                    foreach (StaffMember staff in staffs)
                    {
                        string staffZoomId;
                        if (!userDict.TryGetValue(staff.Email, out staffZoomId))
                        {
                            Console.WriteLine($"Staff [{staff.Name}] do not have zoom account.");
                            continue;
                        }

                        Console.WriteLine($"Zoom ID for [{staff.Name}] is {staffZoomId}");
                        ZoomKit.createMeeting(act, staff, staffZoomId, tokenString);
                    }
                }

                splus.SDB.RefreshAndWriteBack();
            }

            return (int)ExitCode.Success;
        }

        static List<string> PreConnectionValidation(Options o)
        {
            var validationErrors = new List<string>();

            /*
            if (!String.IsNullOrEmpty(o.Activities) && !String.IsNullOrEmpty(o.TeachingActivities))
            {
                validationErrors.Add("  * Both --activities and --teaching-activities detected but only one is allowed.");
            }
            */

            return validationErrors;
        }

        private List<string> PostConnectionValidation(College college)
        {
            var validationErrors = new List<string>();
            /*
            // When allow-drop-off isn't supplied, then we must make sure that the
            // target range falls within the range of the image.
            if (!_options.AllowDropOff)
            {
                if (_options.Start + _options.ShiftBy < 0 || _options.End + _options.ShiftBy > (college.WeeksPerYear - 1))
                {
                    validationErrors.Add(
                        "  * This shift would result in weeks dropping off the end.\n" +
                        "  * To force this shift action, allow it with --allow-drop-off");
                }
            }

            GetTagIfItExists(_options.NamedAvailabilities, college, ref _namedAvailabilityTag, validationErrors);
            GetTagIfItExists(_options.ActivitiesByTag, college, ref _activityTag, validationErrors);
            GetTagIfItExists(_options.ActivitiesByType, college, ref _activityTypeTag, validationErrors);
            GetTagIfItExists(_options.SchActivitiesByType, college, ref _schActivityTypeTag, validationErrors);
            GetModuleGroupIfExists(_options.Modules, college, ref _moduleGroup, validationErrors);
            GetModuleGroupIfExists(_options.ModulesCascading, college, ref _moduleCascadingGroup, validationErrors);
            GetActivityGroupIfExists(_options.Activities, college, ref _activityGroup, validationErrors);
            GetActivityGroupIfExists(_options.TeachingActivities, college, ref _teachingActivityGroup, validationErrors);
            GetActivityTemplateGroupIfExists(_options.ActivityTemplates, college, ref _activityTemplateGroup, validationErrors);

            */

            return validationErrors;
        }

        public static bool checkZoomMeetingExistsFromUserText2(Activity act)
        {
            // check with Regex if UserText2 is a http / zoom meeting link.
            Regex pattern = new Regex(@"^http(s)?://[\w-]+.zoom.us(/[\w- ./?%&=]*)?$");

            return ImageKit.checkPatternExistsFromUserText2(act, pattern);
        }
    }
}
