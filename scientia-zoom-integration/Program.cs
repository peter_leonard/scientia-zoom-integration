﻿using Microsoft.IdentityModel.Tokens;
using Newtonsoft.Json;
using RestSharp;
using SplusServer;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using ScientiaZoomKit;
using CommandLine;

namespace ZoomIntegration
{
    class Program
    {
        public class Options
        {
            [Option('P', "progid", Required = true, HelpText = "COM ProgId of the Syllabus Plus Lisp Image to connect to.")]
            public string ProgId { get; set; }

        }

        static int Main(string[] args)
        {
            var result = (int)ExitCode.UnknownError;
            Console.OutputEncoding = Encoding.UTF8;

            Console.WriteLine(String.Format("Argument is {0}", string.Join(" ", args)));

            var parser = new Parser(config =>
            {
                config.CaseInsensitiveEnumValues = true;
                config.AutoVersion = true;
                config.HelpWriter = Console.Error;
                config.MaximumDisplayWidth = 100;
                config.AutoHelp = false;
                //config.HelpWriter = null;
            });

            parser.ParseArguments<Options>(args).WithParsed((Action<Options>)(o =>
                {
                    Console.WriteLine(String.Format("ProgId is {0}", o.ProgId));
                    var zoom = new Zoom(o);
                    result = zoom.Execute();
                }))
                .WithNotParsed(e =>
                {
                    result = (int)ExitCode.InvalidArgs;
                });

            return result;
        }
    }
}
